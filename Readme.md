# Project Name: *Tally Ho*

## Implementation:
  * HTML
  * CSS
  * JS

## Premise:
  * Design a counter that records and stores number of clicks

## Execution requirements:
  * Any web browser- preferable *Chrome*- as other browsers have not been
    extensively tested


## Completion:
  * Display count and save buttons : *100 %*
  * Increment count on click : *100 %*
  * Save current count : *100 %*
  * Display current tally : *100 %*
  * Display previous saved tallies : *100 %*



## Optional Completions:
  * Display session total tally : *100 %*
  * Store timestamps of tally saves : *100 %*
  * Display "View Save Log" button : *100 %*
  * Toggle text on "View Save Log" on click : *100 %*
  * Display session save log to screen on click : *100 %*
  * Hide session save log on click : *100 %*
  * Collect email to send session save log : *0 %*
  * Bundle session save log and send to email : *0 %*



