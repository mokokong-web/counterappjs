
// * CORE CONCEPTS
// Functions
// Mathematical operations
// Using console log
// Event listening
// Displaying to html using a function


// Buttons elements
let saveBtn = document.getElementById("save-btn")
let incrementBtn = document.getElementById("increment-btn")
let logBtn = document.getElementById("log-btn")
let saveLogDiv = document.getElementById("save-log")

 
//   Display elements 
let logClass = document.querySelector("#log-btn")
let welcomeEl = document.getElementById("welcome-el")
let countEl = document.getElementById("count-el")
let saveEl = document.getElementById("save-el")
let totalEl = document.getElementById("total-el")

let count = 0
let pEntries = ""
let total = 0

let name = "Player 1"
let greeting = "Welcome to Tally Ho, "
let welcomeMessage = greeting + name
welcomeEl.innerText = welcomeMessage

let saveLog = {}

// Functions

function tallyCount(){

    count +=1
    countEl.innerText = count
    console.log(count)
}

function saveTallyCount(){

    //log timestamp and update savelog
    var timestamp = new Date(Date.now())
    saveLog[timestamp] = count

    // console.log(saveLog)

    //update total and previous entries
    pEntries  += count + " - "
    total += count
    
    saveEl.innerText ="Previous Entries :" + pEntries
    totalEl.innerText = total
    

    // clear count and set it back to 0
    count = 0
    countEl.innerText = count

}

function toggleLog(){

    if (logClass.classList.contains("view-log")){

        // change text to hide save log
        logBtn.textContent = "HIDE SAVE LOG"
        logClass.classList.replace("view-log", "hide-log")

        
        console.log(JSON.stringify(saveLog,null, '\t'))
        saveLogDiv.innerText = JSON.stringify(saveLog,null, '\t')


    }
    else
    {
        // change text to view save log
        logBtn.textContent = "VIEW SAVE LOG"
        logClass.classList.replace("hide-log","view-log")

        saveLogDiv.innerText = ""
    }

}

// Eventlisteners
saveBtn.addEventListener("click",saveTallyCount)

incrementBtn.addEventListener("click",tallyCount)

logBtn.addEventListener("click",toggleLog)
